# El Reto

Define una variable llamada `aVariable`, asígnale el valor de `'I am a variable!'` e imprímela a través de la salida estándar.

# Info

## Variables

### A la vieja escuela

```javascript
var aVariable = 67
var declared

function aFunction () {
  if (true) {
    var anotherVariable = 'yeps'
  }
  return anotherVariable
}
```

* __Dos tipos de alcance:__ Las variables declaradas de esta manera son _globales_ (se pueden referenciar desde cualquier lugar del programa) como __aVariable__, o _de alcance de función_ (cualquier se puede referenciar desde cualquier parte dentro de la función, incluyendo otras funciones), en el caso de __anotherVariable__, podemos referenciarla en cualquier lugar dentro de __a_function__.

### La nueva forma

```javascript
const aVariable = 67
let declared

function a_function () {
  if (true) {
    let anotherVariable = 'yeps'
  }
  return 'nops'
}
```

* __Declaración de constantes:__ Las _constantes_ se declaran como valores que no van a poder cambiar durante la ejecución del programa, en este caso, __aVariable__ siempre tendrá el valor de `67`.
* __Alcance de Bloque:__ Las nuevas declaraciones (tanto `let` como `const`) tienen alcance de _bloque_, es decir que sólo el bloque en el que están declaradas tienen acceso a ellas, en el caso de __anotherVariable__, solo se puede referenciar dentro del bloque `if`.

### Características comunes

* __Levemente tipado:__ Las variables en _JavaScript_ son _levemente_ tipadas, es decir que hay tipos, pero el lenguaje los infiere, ademas de eso, para _JavaScript_ no existe por ejemplo, distinción entre `int` o `float`, todos los números son `Numbers`.
* __Declaraciones sin asignación:__ Una variable es básicamente una referencia a un valor, en _Javascript_ podemos declarar una variable sin asignarle un valor, y asignárselo luego, cuando sea necesario, este es el caso en ambos ejemplos para la variable __declared__.

## Salida estándar

_JavaScript_ ofrece una interfaz sencilla para enviar mensajes a la __salida estándar__ y al __error estándar__ llamada `console`.

* Para escribir a la salida __salida estándar__ utiliza `console.log('Going to stdout')`

* Para escribir al __error estándar__ utiliza `console.error('Going to sdterr')`