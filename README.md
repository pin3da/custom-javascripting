# Introducción

Estos son una serie de retos basados en el workshop __javascripting__, con el propósito de entrenar personas nuevas en el lenguaje, y como parte de un curso dirigido en el __Nodeschool Day__.

# Retos

1. [Calentando](1_warming_up)
